# Support moves with up to 16 characters

Pokémon move names used to be up to 12 characters long, this was changed in Generation 6 and newer moves have names up to 16 characters.

In this tutorial, we'll expand move names to 16 character and adapt all relevant menus to correctly print the longer names.

## Contents

1. [Change the move length constant](#1-change-the-move-length-constant)
2. [Correct the green page in the stats screen](#2-correct-the-green-page-in-the-stats-screen)
3. [Enlarge the move selection menu](#3-enlarge-the-move-selection-menu)
4. [Enlarge the "forget move" menu](#4-enlarge-the-forget-move-menu)
5. [Fix the "mon used move" text](#5-fix-the-mon-used-move-text)
6. [Fix the TM/HM pocket](#6-fix-the-tmhm-pocket)
7. [Various text fixes](#7-various-text-fixes)

## 1. Change the move length constant and update move names

Edit [constants/text_constants.asm](../blob/master/constants/text_constants.asm):

``` diff
-DEF MOVE_NAME_LENGTH          EQU 13
+DEF MOVE_NAME_LENGTH          EQU 17
```

The length constant includes the termination character, so we need it to be 1 more than the desired length.

Edit [ram/wram.asm](../blob/master/ram/wram.asm):

``` diff
 SECTION "More WRAM 1", WRAMX
 
 wTMHMMoveNameBackup:: ds MOVE_NAME_LENGTH
 
 wStringBuffer1:: ds STRING_BUFFER_LENGTH
 wStringBuffer2:: ds STRING_BUFFER_LENGTH
 wStringBuffer3:: ds STRING_BUFFER_LENGTH
 wStringBuffer4:: ds STRING_BUFFER_LENGTH
 wStringBuffer5:: ds STRING_BUFFER_LENGTH

 wBattleMenuCursorPosition:: db

-   ds 1

 wCurBattleMon::
  ; index of the player's mon currently in battle (0-5)
    db
 
 ...

 wPCItemsCursor::        db
 wPartyMenuCursor::      db
 wItemsPocketCursor::    db
 wKeyItemsPocketCursor:: db
 wBallsPocketCursor::    db
 wTMHMPocketCursor::     db

 wPCItemsScrollPosition::        db
-   ds 1
 wItemsPocketScrollPosition::    db
 wKeyItemsPocketScrollPosition:: db
 wBallsPocketScrollPosition::    db
 wTMHMPocketScrollPosition::     db

 ...
 
 wItemQuantityChange:: db
 wItemQuantity:: db

 wTempMon:: party_struct wTempMon

 wSpriteFlags:: db

 wHandlePlayerStep:: db

-   ds 1

 wPartyMenuActionText:: db

 wItemAttributeValue:: db

 wCurPartyLevel:: db

 wScrollingMenuListSize:: db

-   ds 1
...
```

Since we increased `MOVE_NAME_LENGTH` by 4, we are expanding the space reserved for `wTMHMMoveNameBackup` in wram, so we need to remove 4 bytes of otherwise unused space from the same section.

Edit [home/names.asm](../blob/master/home/names/asm):

``` diff
 GetName::
 ...

 .NotPokeName
    ld a, [wNamedObjectType]
    dec a
    ld e, a
    ld d, 0
    ld hl, NamesPointers
    add hl, de
    add hl, de
    add hl, de
    ld a, [hli]
    rst Bankswitch
    ld a, [hli]
    ld h, [hl]
    ld l, a

    ld a, [wCurSpecies]
    dec a
    call GetNthString

    ld de, wStringBuffer1
-   ld bc, ITEM_NAME_LENGTH
+   ld bc, MOVE_NAME_LENGTH
    call CopyBytes
```

`ITEM_NAME_LENGTH` and `MOVE_NAME_LENGTH` used to have the same value, but now `MOVE_NAME_LENGTH` is bigger.
We need to change the code above or names longer than 12 characters will not be correctly copied in ram when used.
Since this code is copying into ```wStringBuffer1``` which has ```STRING_BUFFER_LENGTH``` (=19) reserved for it, we don't need to change anything else.

The only menu that will work out of the box is the "MOVE" menu in the party, other menus are addressed in the following sections.

![move menu](../../raw/main/screenshots/move-menu.png)

## 2. Correct the green page in the stats screen

If we try to print a move name longer than 12 characters in the stats screen right now, it will awkwardly break the name and print part of it on the next line.

To correct this we will remove the "MOVE" text on the left of the screen and print the move names 4 tiles to the left.

Edit [engine/pokemon/stats_screen.asm](../blob/master/engine/pokemon/stats_screen.asm):

``` diff
 LoadGreenPage:
    ld de, .Item
    hlcoord 0, 8
    call PlaceString
    call .GetItemName
    hlcoord 8, 8
    call PlaceString
-   ld de, .Move
-   hlcoord 0, 10
-   call PlaceString
    ld hl, wTempMonMoves
    ld de, wListMoves_MoveIndicesBuffer
    ld bc, NUM_MOVES
    call CopyBytes
-   hlcoord 8, 10
+   hlcoord 4, 10
    ld a, SCREEN_WIDTH * 2
    ld [wListMovesLineSpacing], a
    predef ListMoves
    hlcoord 12, 11
    ld a, SCREEN_WIDTH * 2
    ld [wListMovesLineSpacing], a
    predef ListMovePP
    ret
    
 ...
    
    
 .ThreeDashes:
    db "---@"

-.Move:
-   db "MOVE@"
```
Now the green page looks like this:

![green page](../../raw/main/screenshots/green-page.png)

## 3. Enlarge the move selection menu

We will expand the menu border to the edges of the screen, move the cursor to the left and start printing the move names immediately after the cursor.

Edit [engine/battle/core.asm](../blob/master/engine/battle/core.asm):

``` diff
MoveSelectionScreen:
    ...

.got_menu_type
    ld de, wListMoves_MoveIndicesBuffer
    ld bc, NUM_MOVES
    call CopyBytes
    xor a
    ldh [hBGMapMode], a

-   hlcoord 4, 17 - NUM_MOVES - 1
+   hlcoord 0, 17 - NUM_MOVES - 1
    ld b, 4
-   ld c, 14
+   ld c, 18
    ld a, [wMoveSelectionMenuType]
    cp $2
    jr nz, .got_dims
-   hlcoord 4, 17 - NUM_MOVES - 1 - 4
+   hlcoord 0, 17 - NUM_MOVES - 1 - 4
    ld b, 4
-   ld c, 14
+   ld c, 18
.got_dims
    call Textbox

-   hlcoord 6, 17 - NUM_MOVES
+   hlcoord 2, 17 - NUM_MOVES
    ld a, [wMoveSelectionMenuType]
    cp $2
    jr nz, .got_start_coord
-   hlcoord 6, 17 - NUM_MOVES - 4
+   hlcoord 2, 17 - NUM_MOVES - 4
.got_start_coord
    ld a, SCREEN_WIDTH
    ld [wListMovesLineSpacing], a
    predef ListMoves

-   ld b, 5
+   ld b, 1
    ld a, [wMoveSelectionMenuType]
    cp $2
    ld a, 17 - NUM_MOVES
    jr nz, .got_default_coord
-   ld b, 5
+   ld b, 1
    ld a, 17 - NUM_MOVES - 4
    
    ...
    
.battle_player_moves
    call MoveInfoBox
    ld a, [wSwappingMove]
    and a
    jr z, .interpret_joypad
-   hlcoord 5, 13
+   hlcoord 1, 13
    ld bc, SCREEN_WIDTH
    dec a
    call AddNTimes
    ld [hl], "▷"
```
This menu isn't used just during move selection in battle, but also to select the move to restore/upgrade with Ether, PP-Up and similar items.
In that case the sprites of the Pokémon in the party screen will be drawn over the menu box.

Specifically, the sprites of the last two Pokémon in the party, which are items 16 to 23 in [OAM](https://gbdev.io/pandocs/OAM.html) need to disappear.
To do that we will simply move them off-screen by setting their Y coordinate to 0 before drawing the menu.

There is no need to move them back, because the entire party screen is reloaded when the menu is closed.

Edit [engine/battle/core.asm](../blob/master/engine/battle/core.asm):

``` diff
 MoveSelectionScreen:
    call IsMobileBattle
    jr nz, .not_mobile
    farcall Mobile_MoveSelectionScreen
    ret

 .not_mobile
    ld hl, wEnemyMonMoves
    ld a, [wMoveSelectionMenuType]
    dec a
    jr z, .got_menu_type
    dec a
    jr z, .ether_elixer_menu
    call CheckPlayerHasUsableMoves
    ret z ; use Struggle
    ld hl, wBattleMonMoves
    jr .got_menu_type

 .ether_elixer_menu
+   ld hl, (wShadowOAM + (16 * 4))
+   xor a
+   ld b, 8
+.loop
+   ld [hli], a
+   inc hl
+   inc hl
+   inc hl
+   dec b
+   jr nz, .loop
    ld a, MON_MOVES
    call GetPartyParamLocation
```

The menu looks like this in battle and when using an Ether.

![move selection battle](../../raw/main/screenshots/move-selection-battle.png)
![move selection ether](../../raw/main/screenshots/move-selection-ether.png)

## 4. Enlarge the "forget move" menu

Once again, we expand the menu to the edges of the screen.

Edit [engine/pokemon/learn.asm](../blob/master/engine/pokemon/learn.asm):

``` diff
 ForgetMove:
 ...
 .loop
    push hl
    ld hl, MoveAskForgetText
    call PrintText
-   hlcoord 5, 2
+   hlcoord 0, 2
    ld b, NUM_MOVES * 2
-   ld c, MOVE_NAME_LENGTH
+   ld c, MOVE_NAME_LENGTH + 1
    call Textbox
-   hlcoord 5 + 2, 2 + 2
+   hlcoord 0 + 2, 2 + 2
    ld a, SCREEN_WIDTH * 2
    ld [wListMovesLineSpacing], a
    predef ListMoves
    ; w2DMenuData
    ld a, $4
    ld [w2DMenuCursorInitY], a
-   ld a, $6
+   ld a, $1
    ld [w2DMenuCursorInitX], a
```
Similarly to the previous section, this menu is used in the party screen when a Pokémon learns a move via TM or Rare Candy, and sprites of Pokémon 2-6 are drawn over it.

We can move the sprites out of the way like before, however:
- the forget move menu is opened and closed before the party screen disappears, which means if we move them away we need to move them back.
- all sprites except the first are affected, which makes said movements more expensive than before.

Because of these reasons I prefer the approach of disabling the entire sprite layer by using the [LCD Control register](https://gbdev.io/pandocs/LCDC.html).

It's also possible to shrink the height of this menu, making is as large as the previous one.

Edit [engine/pokemon/learn.asm](../blob/master/engine/pokemon/learn.asm):

``` diff
 ForgetMove:
 ...
 .loop
    push hl
    ld hl, MoveAskForgetText
    call PrintText
+   ldh a, [rLCDC]
+   and ~(1 << rLCDC_SPRITES_ENABLE)
+   ldh [rLCDC], a 
    ...
    jr c, .hmmove
    pop hl
    add hl, bc
+   push af
+   ldh a, [rLCDC]
+   or (1 << rLCDC_SPRITES_ENABLE)
+   ldh [rLCDC], a
+   pop af
    and a
    ret

.hmmove
+   ldh a, [rLCDC]
+   or (1 << rLCDC_SPRITES_ENABLE)
+   ldh [rLCDC], a
    ld hl, MoveCantForgetHMText
    call PrintText
    pop hl
-   jr .loop
+   jp .loop

.cancel
+   ldh a, [rLCDC]
+   or (1 << rLCDC_SPRITES_ENABLE)
+   ldh [rLCDC], a
    scf
    ret
```

The forget move menu looks like this in battle and in the party screen:

![forget move menu battle](../../raw/main/screenshots/move-forget-menu-battle.png)
![forget move menu party](../../raw/main/screenshots/move-forget-menu-party.png)

## 5. Fix the "mon used move" text

The "mon used move" text in battle is able to print names long up to 12 characters without breaking, for longer names we will need to break the line.

Since this is one more button press on a very frequently seen textbox it's worth it to only break the line when necessary.

To do this, we will adapt the unused move grammar table and use it to maintain a list of moves with long names that we will be using again later.

Edit [data/moves/grammar.asm](../blob/master/data/moves/grammar.asm), removing everything except the initial label and the list terminator ```db -1```.
You can add items to this list with ```db``` before the terminator.

If, for example, you define Double Iron Bash following the [new move tutorial](https://github.com/pret/pokecrystal/wiki/Add-a-new-move), your file will look like this.

``` diff
MoveGrammar:
    db DOUBLE_IRON_BASH
    db -1 ; end
```
Every move with a name longer than 12 characters needs to be added to this list.

Edit [data/text/common_2.asm](../blob/master/data/text/common_2.asm):

``` diff
 _UsedMove2Text::
    text_start
-   line "used @"
+   line "used"
+   cont "@"
    text_end
```

Now we want to modify the code that prints the used move text to use `UsedMove2Text` when a move is in the `MoveGrammar` list and `UsedMove1Text` otherwise.

Edit [engine/battle/used_move_text.asm](../blob/master/engine/battle/used_move_text.asm):

``` diff
 UsedMoveText:
 ...
 .grammar
    call GetMoveGrammar ; convert move id to grammar index

 ; everything except 'CheckObedience' made redundant in localization

    ; check obedience
    ld a, [wAlreadyDisobeyed]
    and a
    ld hl, UsedMove2Text
    ret nz

    ; check move grammar
    ld a, [wMoveGrammar]
-   cp $3
-   ld hl, UsedMove2Text
-   ret c
+   and a
    ld hl, UsedMove1Text
+   ret z
+   ld hl, UsedMove2Text
    ret

...

 GetMoveGrammar:
 ; store move grammar type in wMoveGrammar

    push bc
 ; wMoveGrammar contains move id
    ld a, [wMoveGrammar]
    ld c, a ; move id
    ld b, 0 ; grammar index

 ; read grammar table
    ld hl, MoveGrammar
 .loop
    ld a, [hli]
 ; end of table?
    cp -1
    jr z, .end
 ; match?
    cp c
-   jr z, .end
-; advance grammar type at 0
-   and a
    jr nz, .loop
-; next grammar type
    inc b
-   jr .loop

 .end
 ; wMoveGrammar now contains move grammar
    ld a, b
    ld [wMoveGrammar], a

; we're done
    pop bc
    ret
```

`GetMoveGrammar` will set `wMoveGrammar` to 1 if the move is on the list and 0 otherwise, then `UsedMoveText` will use that value to choose which text to use.

The text looks like normal on short names and like this on long ones:

![used move text 1](../../raw/main/screenshots/used-move-text1.png)
![used move text 2](../../raw/main/screenshots/used-move-text2.png)

## 6. Fix the TM/HM pocket

Inside the TM/HM pocket, names of moves longer than 12 characters are printed incorrectly just like the menus we fixed before.

We will once again use the move grammar table, plus a new table that will contain strings meant to print the move name on two rows.

Edit [engine/battle/used_move_text.asm](../blob/master/engine/battle/used_move_text.asm):

``` diff
 MoveNameText:
    text_far _MoveNameText
    text_asm
 ; get start address
    ld hl, .endusedmovetexts
-
-; get move id
-   ld a, [wMoveGrammar]
-
-; 2-byte pointer
-   add a
-
-; seek
-   push bc
-   ld b, 0
-   ld c, a
-   add hl, bc
-   pop bc
-
 ; get pointer to usedmovetext ender
    ld a, [hli]
    ld h, [hl]
    ld l, a
    ret
    
 .endusedmovetexts
 ; entries correspond to MoveGrammar sets
	dw EndUsedMove1Text
-	dw EndUsedMove2Text
-	dw EndUsedMove3Text
-	dw EndUsedMove4Text
-	dw EndUsedMove5Text

 EndUsedMove1Text:
	text_far _EndUsedMove1Text
	text_end

-EndUsedMove2Text:
-	text_far _EndUsedMove2Text
-	text_end

-EndUsedMove3Text:
-	text_far _EndUsedMove3Text
-	text_end

-EndUsedMove4Text:
-	text_far _EndUsedMove4Text
-	text_end

-EndUsedMove5Text:
-	text_far _EndUsedMove5Text
-	text_end

...

 GetMoveGrammar:
 ...
 .loop
+   inc b
    ld a, [hli]
 ; end of table?
    cp -1
-   jr z, .end
+   jr z, .not_found
 ; match?
    cp c
    jr nz, .loop
-   inc b

 .end
-; wMoveGrammar now contains move grammar
+; wMoveGrammar now contains move grammar index
    ld a, b
    ld [wMoveGrammar], a

 ; we're done
    pop bc
    ret

+.not_found
+   ld b, 0
+   jr .end
```

We changed `GetMoveGrammar` to increase `b` each time we pass a byte in the move grammar table, effectively counting how many steps we need to reach our move, while setting it to 0 like before when the move is not on the table.

We don't need to change `UsedMoveText` since that just checks if `wMoveGrammar` is 0 or not, however `MoveNameText` uses `wMoveGrammar` to select which string is used to terminate the used move message, but since they're all the same we can avoid the selection and always use the first one.

Since we're only using the first of the `EndUsedMoveNText`, we can delete all the others.

Edit [data/text/common_2.asm](../blob/master/data/text/common_2.asm):

```diff
 _EndUsedMove1Text::
	text "!"
	done

-_EndUsedMove2Text::
-	text "!"
-	done

-_EndUsedMove3Text::
-	text "!"
-	done

-_EndUsedMove4Text::
-	text "!"
-	done

-_EndUsedMove5Text::
-	text "!"
-	done
```

Create **data/moves/two_lines.asm**.

It should contain the `TwoLinesPointers:` label followed by a pointer list, then followed by the strings, similar to [data/moves/descriptions.asm](../blob/master/data/moves/descriptions.asm).

The strings must have an `@` where you want the line break and an `@` at the end.

The pointer list must have the same length as the list in [data/moves/grammar.asm](../blob/master/data/moves/grammar.asm) and it must follow the same order.

For example, if your [data/moves/grammar.asm](../blob/master/data/moves/grammar.asm) looks like this:

``` diff
MoveGrammar:
    db ELECTRIC_TERRAIN
    db DOUBLE_IRON_BASH
    db -1
```

your **data/moves/two_lines.asm** should look like this:

``` diff
TwoLinesPointers:
    dw ElectricTerrain2L
    dw DoubleIronBash2L
    
ElectricTerrain2L:
    db "ELECTRIC@TERRAIN@"

DoubleIronBash2L:
    db "DOUBLE IRON@BASH@"
```

Edit [engine/items/tmhm.asm](../blob/master/engine/items/tmhm.asm):

``` diff
 TMHM_DisplayPocketItems:
    ...
 .okay
    predef GetTMHMMove
    ld a, [wNamedObjectIndex]
    ld [wPutativeTMHMMove], a
    call GetMoveName
    pop hl
    ld bc, 3
    add hl, bc
    push hl
-   call PlaceString
+   call PlaceTMHMName
    ...
 .done
    ret
     
+PlaceTMHMName:
+   push hl
+   ld a, [wPutativeTMHMMove]
+   ld [wMoveGrammar], a
+   callfar GetMoveGrammar
+   ld a, [wMoveGrammar]
+   pop hl
+   and a
+   jr nz, .two_lines
+   jp PlaceString
+
+.two_lines
+   push hl
+   push bc
+   ld hl, TwoLinesPointers
+   ld a, [wMoveGrammar]
+   dec a
+   add a
+   ld b, 0
+   ld c, a
+   add hl, bc
+
+.print
+   pop bc
+   ld a, [hli]
+   ld e, a
+   ld a, [hl]
+   ld d, a
+   pop hl
+   push hl
+   call PlaceString
+   pop hl
+   push bc
+   ld b, 0
+   ld c, SCREEN_WIDTH
+   add hl, bc
+   pop bc
+   inc de
+   jp PlaceString
+
+INCLUDE "data/moves/two_lines.asm"
```

The new `PlaceTMHMName` checks if a move is in the grammar list, if not it does `PlaceString` as it did before, otherwise it reads the corresponding entry in the `TwoLinesPointers` list and prints the pointed string using two `PrintString`s and a bit of coord management.

The TM/HM Pocket now looks like this:

![tmhm pocket](../../raw/main/screenshots/tmhm-bag.png)

## 7. Various text fixes

The final step is to change a few strings to have the move name be the only thing in a line.

Edit [data/text/common_2.asm](../blob/master/data/text/common_2.asm):

``` diff
 _ContainedMoveText::
    text_ram wStringBuffer2
    text "."
    
-   para "Teach @"
+   para "Teach"
+   line "@"
    text_ram wStringBuffer2
    text_start
-   line "to a #MON?"
+   cont "to a #MON?"
    text_end
    
 _TMHMNotCompatibleText::
    text_ram wStringBuffer2
-   text " is"
-   line "not compatible"
+   text ""
+   line "is not compatible"
    cont "with @"
    text_ram wStringBuffer1
    text "."
```

Edit [data/text/common_3.asm](../blob/master/data/text/common_3.asm):

``` diff
 _AskForgetMoveText::
 ...
-   para "Delete an older"
-   line "move to make room"
-   cont "for @"
+   para "Delete a move to"
+   line "make room for"
+   cont "@"
```

Results look like this:

![move not compatible 1](../../raw/main/screenshots/move-not-compatible1.png)
![move not compatible 2](../../raw/main/screenshots/move-not-compatible2.png)

![delete move 1](../../raw/main/screenshots/delete-move1.png)
![delete move 2](../../raw/main/screenshots/delete-move2.png)

![teach move text 1](../../raw/main/screenshots/teach-move-text1.png)
![teach move text 2](../../raw/main/screenshots/teach-move-text2.png)
